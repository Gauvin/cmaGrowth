# Introduction

Experiments with spatial interpolation. Three methods were compared:

- Areal interpolation
- Raster based
- Number of buildings 

The buildings dataset used is from [Microsoft Buildings Open Data] (https://github.com/microsoft/CanadianBuildingFootprints).

The methodology was applied on the Ottawa CMA - 505, which spans parts of Ontario and Quebec. 




