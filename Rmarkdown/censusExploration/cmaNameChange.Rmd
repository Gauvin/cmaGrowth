---
title: "Untitled"
output: html_document
---


# Libs
```{r}

library(cancensus)
library(Census2SfSp)
library(magrittr)
library(tidyverse) 
library(glue)

```

#Params
```{r}

Census2SfSp:::setCancensusOptions()
listDataSets <- cancensus::list_census_datasets() %>% head(5) %>% pull(dataset)
listDataSets

```

# Get the CMAs/CSD for each of the available datasets

```{r}

getUniqueNames <- function(dataSet, level='CMA'){
  df <- cancensus::list_census_regions(dataSet)
  
  cmaNames <- df %>% 
    filter(level ==level) %>% 
    select(name,region)
  

  cmaNames$dataset <- dataSet 
  
  
  cmaNames %<>% mutate_all(as.character)
  cmaNames$region <- as.character(cmaNames$region) #wtf
  
  return(cmaNames)
}


dfCmas <- map_dfr( listDataSets, getNames ) 
```

# Identify CMAs with names that changed or were not classified as cmas in some of the censuses
```{r}

dfByName <- dfCmas %>% 
  group_by(region) %>% 
  count(name) %>% 
  arrange(desc(n))

dfByName %<>% mutate(name=factor(name,levels = as.character(dfByName$name)))

ggplot(dfByName) + 
  geom_col(aes(x=name,y=n))+
  theme(axis.text.x = element_text(angle=90))

```


```{r}

dfByName %>% filter(grepl('Ottawa', name))

```



# Identify CMAs with codes that were not classified as cmas in some of the censuses
```{r}

dfByRegion <- dfCmas %>% 
  group_by(region) %>% 
  summarise(n=n_distinct(dataset)) %>% 
  arrange(desc(n))

dfByRegion %<>% mutate(region=factor(region,levels = as.character(dfByRegion$region)))

ggplot(dfByRegion) + 
  geom_col(aes(x=region,y=n))+
  theme(axis.text.x = element_text(angle=90))

```

```{r}

getCMA <- function( dataset, cma ){
  shpCensus <- cancensus::get_census(dataset,
                        regions = list(CMA=cma), 
                        level = 'CMA',
                        geo_format = 'sf') %>% 
    dplyr::select(Population, `Shape Area`, geometry,name) 
  
  shpCensus$dataset <- dataset  
  
  return(shpCensus)}


```

# Plot the change in CMA size and shape according to different censuses
```{r}

plotAnalyzeCMA <- function(cma){
   
  listCMAGeo <- map(listDataSets, 
                   ~getCMA(.x,cma))
  
  shpAllCensuses <- do.call(rbind , listCMAGeo)  
  shpAllCensuses %<>% mutate(dataset =factor(dataset, levels=as.character(shpAllCensuses$dataset) ))
  
  shpAllCensuses$popDiff <- c(NA, shpAllCensuses$Population[2:nrow(shpAllCensuses)]-shpAllCensuses$Population[1:nrow(shpAllCensuses)-1] )
  
  shpAllCensuses %<>% mutate(popPerDiff = 100*(popDiff/Population))
  
  name <- paste( unique(shpAllCensuses$name)[[1]], sep=",", collapse = "," )
  
  ggplot(shpAllCensuses) + 
    geom_sf(aes(fill=Population))  +
    facet_wrap(~dataset) +
    viridis::scale_fill_viridis() + 
    theme_bw()+
    geom_sf_label (aes(label=glue("Pop (mil.): {round(Population/10**6,2)}\nPop incr (%): {round(popPerDiff,2)}"))) +  
    ggtitle(glue("{name} CMA - geographic and demographic evolution"))
  
 
}

```

```{r}

dfTopCMAs <- cancensus::list_census_regions('CA16') %>% 
  filter(level=='CMA') %>% 
  top_n(n=10,wt=pop)

listCMAS <- dfTopCMAs %>% pull(region)

listPlots <- map( listCMAS,  plotAnalyzeCMA )

```
