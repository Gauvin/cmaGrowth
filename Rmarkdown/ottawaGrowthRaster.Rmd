---
title: "Untitled"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


# Libs
```{r}

library(here)
library(sf)
library(stars)
library(areal)

library(cancensus)
library(Census2SfSp)
library(magrittr)
library(tidyverse) 
library(glue)

library(raster)

library(snapbox)


R.utils::sourceDirectory(here("R"))

library(tongfen)

```

#Params
```{r}

Census2SfSp:::setCancensusOptions( )


listDataSets <- cancensus::list_census_datasets() %>% head(5) %>% pull(dataset)
listDataSets
  
listYears <- map_chr(listDataSets , ~substr(.x, nchar(.x)-1, nchar(.x)))

proj102002 <- "+proj=lcc +lat_1=50 +lat_2=70 +lat_0=40 +lon_0=-96 +x_0=0 +y_0=0 +datum=NAD83 +units=m +no_defs"
  
mapBoxToken <- keyringr::decrypt_gk_pw('token mapToken')
Sys.setenv(MAPBOX_ACCESS_TOKEN=mapBoxToken)
 
```

---
---

# Data input

## Read the intersected geometries data

### Get the list of shp + reproject
```{r}

listShp <- map( listDataSets, 
                ~cancensus::get_census(.x,regions = list(CMA='505'), level = 'DA',geo_format = 'sf') )

#Reproject + add the id as concat of census dataset and geouid
for(k in 1:length(listShp)){
  listShp[[k]] %<>% st_transform(crs=proj102002)
  listShp[[k]][[glue('id{listDataSets[[k]]}')]] <-  paste(listDataSets[[k]], listShp[[k]]$GeoUID, sep="_")
}

listShp %<>% setNames(listDataSets)

```


## Read the buildings data

```{r}


  mypwd <- keyringr::decrypt_gk_pw("user charles")

  #ottawa_buildings
  conn <- DBI::dbConnect( drv = RPostgreSQL::PostgreSQL(),
                          host="localhost",
                          user="postgres",
                          password=mypwd,
                          dbname="ms_buildings")

  #Read + Transform to proj102002
  queryStr <- 'SELECT *
              FROM cma_ottawa_allcma_buildings'
  
  shpBuildings <- sf::st_read(conn,
                              query = queryStr) %>% 
    st_transform(crs=proj102002)

```




#Get the core Ottawa limits
```{r}

shpOttawaCore <- get_census('CA1996' ,
                            regions = list(CSD='3506014'),
                            level = 'CSD', 
                            geo_format = 'sf') 

```

---
---

# Building density 

## Compute the 2D KDE estimation

```{r}

cell <- 10**3
bw <- cell/2

rastKde <- SfSpHelpers::st_kde(shpBuildings, 
                    cellsize = cell,
                    bandwith = bw   )

plot(rastKde)

pathRaster <- here("Data","Raster",glue('rastKde_ottawa_{cell}_{bw}.tif'))
writeRaster(rastKde, pathRaster,overwrite=T)



```

---
---

# Polygonize the raster (turns out this is not really helpful as all geometries intersect this new polygon)

## Binary raster

```{r}

cutoff <- quantile(values(rastKde),probs=0.75)
rastKdeBinary <- reclassify(rastKde, 
           c(-Inf,cutoff,0L, 
             cutoff,Inf,1L))

pathRasterBinary <- here("Data","Raster",glue('binary_rastKde_ottawa_{cell}_{bw}.tif'))
writeRaster(rastKdeBinary, pathRasterBinary,overwrite=T)

```

## Polygonize by calling gdal python script directly
```{r}

shpBinary <- as(rastKdeBinary, "SpatialPixelsDataFrame") %>% st_as_sf

polygonFile <- here('Data','Shp', 'binaryKDEPolygon.shp')

system(glue('rm {polygonFile}')) #overwrite

cmdStr <- glue('gdal_polygonize.py {pathRasterBinary} {polygonFile} -b 1 -f "ESRI Shapefile" OUTPUT DN')
 
system(cmdStr)

shpPolygonFromRast <- st_read( polygonFile ) %>% st_set_crs(st_crs(shpBuildings))

#Remove the old files
filesToRemove <- list.files(path = here('Data','Shp'),
                           pattern = 'binaryKDEPolygon',
                           full.names = T)
map(filesToRemove, file.remove )

#Write the new file with the correct proj file
st_write( shpPolygonFromRast, dsn = here('Data','Shp', 'binaryKDEPolygonWithProj.shp'))
```

---
---

#Create the refined geometry through repeated intersection

## Garbagge collection to free space
```{r}

rm(raster)
rm(rastKdeBinary)
rm(shpBuildings)
gc()


```
  
  
 
## Get the refined geometry

```{r}


# Get the intersection of all censuses
 pwd <- keyringr::decrypt_gk_pw( glue("user charles"))

 
con <- DBI::dbConnect(RPostgres::Postgres(),
                          dbname = 'spatialInterpolation',
                          host='localhost',
                          user='charles',
                          password=pwd
  )
  
  
  #Read + Transform to proj102002
  queryStr <- 'SELECT *
              FROM ottawa_allcma_96_16_intersection'
  
  shpInter <- sf::st_read(con,
                              query = queryStr) %>% 
    st_transform(crs=proj102002)  
  
  
#Get the polygon boundary
shpBoundary <- shpInter %>% st_buffer(100) %>% st_union() %>% st_boundary()

```



---
---




# Get the average building density per (refined intersected) geometry


## Try to speed up by removing geometries that do not intersect (will have 0 density)
```{r}

idxIsZero <- map_lgl( st_intersects( shpInter, shpPolygonFromRast  ), 
                      ~length(.x) == 0)

print(glue("There are {sum(idxIsZero)} features that do not intersect polygonized raster -- removing them"))



shpInter <- shpInter[!idxIsZero,  ]
      
      
```

## Automatic (raster)
```{r, echo=F,message=F,include=F}

tictoc::tic()

#shpSubset <-  shpInter[seq(1, 10**4), ]
shpSubset <- shpInter

#Should have na.rm = T by default
kdeValsExtract <-  raster::extract(rastKde,
                                   shpSubset ,
                                   fun=mean)

finalTime <- tictoc::toc()

shpSubset$weights <- kdeValsExtract
 
```
 



# Interpolate based on these weights

## Actual pop density based interpolation

```{r}

for ( k in 1:length(listDataSets)){
  
  ds <- listDataSets[[k]]
  idStr <- glue('id{ds}')
  weightStr <- glue('weight{ds}')
  popStr <- glue('Population{ds}')
  popWeigthStr <- glue('WeightedPopulation{ds}')
  
  #Group by geometry id for given census and compute the weight 
  shpSubset %<>% 
    group_by_at(vars(one_of(idStr))) %>% 
    mutate( !!weightStr := weights/sum(weights,na.rm = T) )  
    
  shpSubset %<>% ungroup()
  
  #Interpolate population
  shpSubset[[popWeigthStr]] <- shpSubset[[weightStr]] * shpSubset[[popStr]]
  
}


 

```



## Compute population increase
```{r}
 


for(k in 2:length(listDataSets)){
  
    shpSubset[[ glue("WeightedPopulationIncrease{listDataSets[[k]]}")]] <-  shpSubset[[ glue("WeightedPopulation{listDataSets[[k]]}") ]] -  shpSubset[[ glue("WeightedPopulation{listDataSets[[k-1]]}") ]] 
    shpSubset[[ glue("WeightedPopulationIncreaseProp{listDataSets[[k]]}")]] <- shpSubset[[ glue("WeightedPopulationIncrease{listDataSets[[k]]}")]]/shpSubset[[ glue("WeightedPopulation{listDataSets[[k-1]]}") ]] 
}

```
 

## Write to spatialInterpolation postgis db
```{r}



pwd <- keyringr::decrypt_gk_pw( glue("user charles"))

 
con <- DBI::dbConnect(RPostgres::Postgres(),
                          dbname = 'spatialInterpolation',
                          host='localhost',
                          user='charles',
                          password=pwd
  )

 
dbWriteTable(con,
             'ottawa_allcma_96_16_population_raster',
             shpSubset %>% st_transform(crs=4326),
             overwrite=T)

```

---
---

# Validation
```{r}

listGeouids <- substr( shpSubset$idCA1996 , 8, nchar(shpSubset$idCA1996))

shpCensusValid <- listShp[[1]] %>% filter( GeoUID %in% listGeouids)

shpCensusValid %>% dim

shpValid2 <- shpSubset %>% 
  group_by(idCA1996) %>% 
  summarise(PopulationValid = sum(WeightedPopulationCA1996),
            weight= sum(weightCA1996,na.rm = T),
            n = n())
 
assertthat::assert_that( all(dplyr::between(shpValid2$weight, 0-10**-3,1+10**-3) ) )

shpValid2 %<>% left_join(shpCensusValid %>% st_set_geometry(NULL), by='idCA1996')
shpValid2 %<>% mutate(popError = abs(PopulationValid-Population))

assertthat::assert_that( all(dplyr::between(shpValid2 %>% filter(!is.na(popError)) %>% pull(popError), 0, 10**-3) ) )


```

## Investigate a given (rural) geometry 
```{r}

  #Reread the buildings dataset
  mypwd <- keyringr::decrypt_gk_pw("user charles")

  #ottawa_buildings
  conn <- DBI::dbConnect( drv = RPostgreSQL::PostgreSQL(),
                          host="localhost",
                          user="postgres",
                          password=mypwd,
                          dbname="ms_buildings")

  #Read + Transform to proj102002
  queryStr <- 'SELECT *
              FROM cma_ottawa_allcma_buildings'
  
  shpBuildings <- sf::st_read(conn,
                              query = queryStr) %>% 
    st_transform(crs=proj102002)
  
  
  idStr <- 'idCA16'
  
  #This  geometry appears at least twice
  ssMulti <- shpSubset %>% 
    filter_at(vars(idStr),  ~ .=='CA16_24820060' ) %>% 
    dplyr::select(matches(glue('{ds}'))) %>% 
    mutate( area=st_area(.)) %>% 
    mutate( weightedArea = area/sum(area))
  
  ssBinded <- rbind(
    ssMulti %>% dplyr::select(weightedArea) %>% rename(weight=weightedArea) %>%  mutate(id='area'),
    ssMulti %>% dplyr::select(contains(weightStr)) %>% rename(weight=sym(weightStr)) %>% mutate(id='rasterKde')
  )
  
  shpCensusFiltered <- listShp[['CA16']] %>% filter(GeoUID == '24820060')
   
  dfRastCropped <- raster::crop(rastKde, ssBinded) %>% as.data.frame(xy=T)
  
  shpBuildingsFiltered <- SfSpHelpers::st_intersects_filter(shpBuildings , ssBinded )
  
  
```



### Plot
```{r}



  p <- ggplot( ) + 
    #geom_raster(data=dfRastCropped, aes(x=x,y=y,fill=layer)) + 
    snapbox::layer_mapbox( map_style = snapbox::mapbox_light(), st_bbox(shpCensusFiltered), scale_ratio = 0.25)  + 
    geom_sf(data= ssBinded %>% mutate(weight=as.numeric(weight)), aes(fill=weight), alpha=0.5, col='red') + 
    geom_sf(data=shpBuildingsFiltered) + 
    geom_sf(data= shpCensusFiltered, col='red', alpha=0) + 
    coord_sf(datum = NA) + 
    facet_wrap(~id) + 
    scale_fill_viridis_c() + 
    ggtitle("Area vs buildings density interpolation") +
    labs(subtitle='Ottawa CMA - Dissemination area (DA) 24820060',
         caption='Subgeometries partitioning (rural) 24820060 shown.\nEntire 24820060 DA in red and buildings in dark grey.\nBuilding kernel density computed for entire CMA.')
  
  ggsave(here('Figures', 'rasterDensityVsArealOttawaMap.png'),
         p)

  
  p2 <- ggplot()+
      geom_boxplot(data= ssBinded %>% mutate(weight=as.numeric(weight)), aes(x=id, y=weight, col=id), alpha=0.5, col='red') + 
      ggtitle("Area vs buildings density interpolation - weights")  +
      labs(caption='Source: StatCan Census 1996-2016 and Microsoft buildings dataset.\n Stat Can 1996 census accessed with {cancensus}.\nMapbox base map produced by {snapbox}.\nFigure: @charles_gauvin idea credits @CoulSim')
  
  
  ggsave(here('Figures', 'rasterDensityVsArealOttawaDensity.png'),
         p2)

  pBoth <- cowplot::plot_grid( plotlist = list(p,p2),nrow = 2, ncol = 1)
  
  ggsave(here('Figures', 'rasterDensityVsArealOttawaBoth.png'),
         pBoth,
         height = 10)
  
  
```




## ... and suburban one  
```{r}
 
  idStr <- 'idCA16'
  
  #This  geometry appears at least twice
  ssMulti <- shpSubset %>% 
    filter_at(vars(idStr),  ~ .=='CA16_35061677' ) %>% 
    dplyr::select(matches(glue('{ds}'))) %>% 
    mutate( area=st_area(.)) %>% 
    mutate( weightedArea = area/sum(area))
  
  ssBinded <- rbind(
    ssMulti %>% dplyr::select(weightedArea) %>% rename(weight=weightedArea) %>%  mutate(id='area'),
    ssMulti %>% dplyr::select(contains(weightStr)) %>% rename(weight=sym(weightStr)) %>% mutate(id='rasterKde')
  )
  
  shpCensusFiltered <- listShp[['CA16']] %>% filter(GeoUID == '35061677')
   
  dfRastCropped <- raster::crop(rastKde, ssBinded) %>% as.data.frame(xy=T)
    
  shpBuildingsFiltered <- SfSpHelpers::st_intersects_filter(shpBuildings , ssBinded )
  
  
```



### Plot
```{r}



  p <- ggplot( ) + 
    snapbox::layer_mapbox( map_style = snapbox::mapbox_light(), st_bbox(shpCensusFiltered), scale_ratio = 0.25)  + 
    geom_sf(data= ssBinded %>% mutate(weight=as.numeric(weight)), aes(fill=weight), alpha=0.5, col='red') + 
    geom_sf(data=shpBuildingsFiltered) + 
    geom_sf(data= shpCensusFiltered, col='red', alpha=0) + 
    coord_sf(datum = NA) + 
    facet_wrap(~id) + 
    scale_fill_viridis_c() + 
    ggtitle("Area vs buildings density interpolation") +
    labs(subtitle='Ottawa CMA - Dissemination area (DA) 35061677',
         caption='Subgeometries partitioning (suburban) 35061677 shown.\nEntire 35061677 DA in red and buildings in dark grey.\nBuilding kernel density computed for entire CMA.')
  
  ggsave(here('Figures', 'rasterDensityVsArealOttawaMap_v2.png'),
         p)

  
  p2 <- ggplot()+
      geom_boxplot(data= ssBinded %>% mutate(weight=as.numeric(weight)), aes(x=id, y=weight, col=id), alpha=0.5, col='red') + 
      ggtitle("Area vs buildings density interpolation - weights")  +
      labs(caption='Source: StatCan Census 1996-2016 and Microsoft buildings dataset.\n Stat Can 1996 census accessed with {cancensus}.\nMapbox base map produced by {snapbox}.\nFigure: @charles_gauvin idea credits @CoulSim')
  
  
  ggsave(here('Figures', 'rasterDensityVsArealOttawaDensity_v2.png'),
         p2)

  pBoth <- cowplot::plot_grid( plotlist = list(p,p2),nrow = 2, ncol = 1)
  
  ggsave(here('Figures', 'rasterDensityVsArealOttawaBoth_v2.png'),
         pBoth,
         height = 10)
  
  
```



## ... and an urban one  
```{r}
 
  idStr <- 'idCA16'
  
  #This  geometry appears at least twice
  ssMulti <- shpSubset %>% 
    filter_at(vars(idStr),  ~ .=='CA16_35061714' ) %>% 
    dplyr::select(matches(glue('{ds}'))) %>% 
    mutate( area=st_area(.)) %>% 
    mutate( weightedArea = area/sum(area))
  
  ssBinded <- rbind(
    ssMulti %>% dplyr::select(weightedArea) %>% rename(weight=weightedArea) %>%  mutate(id='area'),
    ssMulti %>% dplyr::select(contains(weightStr)) %>% rename(weight=sym(weightStr)) %>% mutate(id='rasterKde')
  )
  
  shpCensusFiltered <- listShp[['CA16']] %>% filter(GeoUID == '35061714')
   
  dfRastCropped <- raster::crop(rastKde, ssBinded) %>% as.data.frame(xy=T)
  
  shpBuildingsFiltered <- SfSpHelpers::st_intersects_filter(shpBuildings , ssBinded )
  
  
```



### Plot
```{r}



  p <- ggplot( ) + 
    snapbox::layer_mapbox( map_style = snapbox::mapbox_light(), st_bbox(shpCensusFiltered), scale_ratio = 0.25)  + 
    geom_sf(data= ssBinded %>% mutate(weight=as.numeric(weight)), aes(fill=weight), alpha=0.5, col='red') + 
    geom_sf(data=shpBuildingsFiltered) + 
    geom_sf(data= shpCensusFiltered, col='red', alpha=0) + 
    coord_sf(datum = NA) + 
    facet_wrap(~id) + 
    scale_fill_viridis_c() + 
    ggtitle("Area vs buildings density interpolation") +
    labs(subtitle='Ottawa CMA - Dissemination area (DA) 35061714',
         caption='Subgeometries partitioning (urban) 35061714 shown.\nEntire 35061714 DA in red and buildings in dark grey.\nBuilding kernel density computed for entire CMA.')
  
  ggsave(here('Figures', 'rasterDensityVsArealOttawaMap_v3.png'),
         p)

  
  p2 <- ggplot()+
      geom_boxplot(data= ssBinded %>% mutate(weight=as.numeric(weight)), aes(x=id, y=weight, col=id), alpha=0.5, col='red') + 
      ggtitle("Area vs buildings density interpolation - weights")  +
      labs(caption='Source: StatCan Census 1996-2016 and Microsoft buildings dataset.\n Stat Can 1996 census accessed with {cancensus}.\nMapbox base map produced by {snapbox}.\nFigure: @charles_gauvin idea credits @CoulSim')
  
  
  ggsave(here('Figures', 'rasterDensityVsArealOttawaDensity_v3.png'),
         p2)

  pBoth <- cowplot::plot_grid( plotlist = list(p,p2),nrow = 2, ncol = 1)
  
  ggsave(here('Figures', 'rasterDensityVsArealOttawaBoth_v3.png'),
         pBoth,
         height = 10)
  
  
```


---
---


# Plot prop increase


## Pop increase (%)

```{r}


listColsPopIncreProp <- grep('WeightedPopulationIncreaseProp' , colnames(shpSubset) , value = T)


shpMelted <- shpSubset %>% 
  dplyr::select(listColsPopIncreProp) %>% 
  pivot_longer(cols = listColsPopIncreProp,
               names_to = 'Census year',  
               values_to='Population increase') %>% 
  mutate_at( vars("Census year") , .funs=function(.x) substr(.x, nchar(.x)-1, nchar(.x)))  %>% 
  st_sf

#Factor labels + ordering
shpMelted %<>% mutate( `Census year`  = plyr::mapvalues(`Census year` ,
                                                        from=c("01" ,"06" ,"11" ,"16"),
                                                        to =c("1996 to 2001" ,"2001 to 2006" ,"2006 to 2011" ,"2011 to 2016") ))

shpMelted %<>% mutate(`Census year` = factor(`Census year`, 
                                             levels = c("1996 to 2001" ,"2001 to 2006" ,"2006 to 2011" ,"2011 to 2016"),
                                             ordered = T))

# Truncate excessively large problematic values
threshValidPropIncrease <- 300/(10**2) %>% as.double() #(1000% is already pretty large)
nBefore <- nrow(shpMelted)
shpMelted %<>% mutate( `Population increase` =pmin(threshValidPropIncrease, `Population increase`) )
nAfter <- nrow(shpMelted)

print(glue('Removed {nBefore - nAfter} records with excessively large increase '))


sum( shpMelted$`Population increase` == threshValidPropIncrease ,na.rm = T)
```

### Actual plot 
```{r}

#Based on centroids to avoid issues with very small geometries + speedup that avoids plotting complex geometries
pMelted <- ggplot() + 
 layer_mapbox(map_style =  stylebox:::mapbox_light() , st_bbox(shpSubset), scale_ratio = 0.25) + 
  geom_sf( data=shpMelted  , aes( fill= `Population increase`),  alpha=0.8, lwd=0  ) + 
  geom_sf( data=shpBoundary , col='black', lwd=0.1) + 
  geom_sf( data=shpOttawaCore, col='red', alpha=0, lwd=0.5) + 
  coord_sf(datum = NA) + 
  facet_wrap(~`Census year`) + 
  theme_bw()  +
  scale_fill_gradient2( labels= scales::percent,
                         low =scales::muted("red"),
                          mid = "white",
                          high = scales::muted("blue"),
                          midpoint = 0) + #diverging color map
  ggtitle("Ottawa year on year population increase") +
  labs( caption = "Source: StatCan Census 1996-2016 and Microsoft buildings dataset. Base map: mapbox from {snapbox}.\nBuildings KDE weighted population based on a refinement\nof the common geometry for all censuses at the DA level.\nCore ottawa city in red - census subdivision of 1996.")

ggsave(here("Figures",'popIncreasePropRasterGeometries.png'),
       pMelted,
       width = 7,
       height = 7)

```

## Raster plot also
```{r}

library(patchwork)

dfKDE <- as.data.frame(rastKde , xy=T)

pRast <- ggplot() + 
  geom_raster(data= dfKDE %>% rename(Density=layer), aes(x=x,y=y,fill=Density)) + 
  geom_sf(data=shpBoundary, lwd=0.5 ) +  #(multi)linestring with 0 alpha: nothing plotted
  geom_sf(data=shpOttawaCore, lwd=0.5, col='red', alpha=0) + 
  scale_fill_viridis_c() + 
  xlab("") + ylab("") + 
  theme_bw() + 
  coord_sf(datum = NA) + 
  ggtitle('2D Kernel density estimation of buildings') + 
  labs(caption = 'Source: Microsoft buildings open dataset.\nCentroids of buildings used to compute KDE.\nCore ottawa city in red - census subdivision of 1996.')

ggsave(here("Figures",'kdeRasterGeometries.png'),
       pRast,
       width = 7,
       height = 7)

pBoth <-  pMelted +  pRast  + plot_layout(nrow = 2,heights  = c(2,1))

ggsave(here("Figures",'popIncreasePropRasterGeometriesBoth.png'),
       pBoth,
       width = 7,
       height = 10)

```

<!-- ## Automatic (terra) -->
<!-- ```{r} -->

<!-- tictoc::tic() -->

<!-- #Should have na.rm = T by default -->
<!-- kdeValsExtract <-  terra::extract(rastKde, -->
<!--                                    shpInter[1:10, ]  , -->
<!--                                    fun=mean) -->

<!-- finalTime <- tictoc::toc() -->
<!-- ``` -->




<!-- ## Helper for manual on-disk computation - slower since we need read and write -->
<!-- ```{r} -->


<!-- getAvgRaster <- function(k,shp,pathRaster){ -->

<!--   avgVal <- 0 -->
<!--   library(magrittr) -->

<!--   tryCatch({ -->


<!--     tempDirFeature <- tempdir( ) -->
<!--     tempFileRaster <- tempfile(fileext = '.tif') -->

<!--     sf::st_write(shp[k, ] ,  -->
<!--                  dsn=tempDirFeature, -->
<!--                  layer= 'cutline',  -->
<!--                  driver='ESRI Shapefile',  -->
<!--                  delete_layer=TRUE) -->

<!--     gdalUtils::gdalwarp(pathRaster, -->
<!--                           tempFileRaster,  -->
<!--                           cutline=tempDirFeature,  -->
<!--                           cl='cutline',  -->
<!--                           dstnodata="NA")  -->

<!--     rast <- terra::rast(pathRaster) -->
<!--     avgVal <- terra::global(rast,'mean') %>% as.numeric() -->

<!--   }) -->


<!--   lapply( list( tempFileRaster), file.remove)  -->
<!--   lapply( list( tempDirFeature), unlink)  -->

<!--   return(avgVal) -->
<!-- } -->


<!-- ``` -->


<!-- ## Manual (parallel) -->
<!-- ```{r, echo=F, message=F, include=F} -->

<!-- tictoc::tic() -->


<!-- Parclusters <- parallel::makeCluster( 6 ) -->

<!-- #Should have na.rm = T by default -->

<!-- kdeValsExtract <- parallel::parLapply( Parclusters, -->
<!--                                        1:10, -->
<!--                                        getAvgRaster, -->
<!--                                        shp=shpInter, -->
<!--                                        pathRaster=pathRaster) -->

<!-- finalTime <- tictoc::toc() -->

<!-- ``` -->

<!-- ```{r} -->


<!-- tictoc::tic() -->


<!-- #Should have na.rm = T by default -->

<!-- kdeValsExtract <- map_dbl(  -->
<!--  1:10,  -->
<!--   ~getAvgRaster(.x,shp=shpInter,pathRaster=pathRaster) -->
<!-- ) -->

<!-- finalTime <- tictoc::toc() -->


<!-- ``` -->

